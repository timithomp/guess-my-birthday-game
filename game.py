from random import randint
#key-value pairs would probably be more readable ie {jan: 31, etc etc}
#month_days = [[January, 31],[February, 28], [March, 31],
#    [April, 30], [May, 31], [June, 30],[July, 31],
#    [August, 31], [September, 30], [October, 31],
#    [November, 30], [December, 31]]



username = input("Hi! What is your name?")
#guess_tracker = []
#month_lower_bound = 1
#month_upper_bound = 12
#year_lower_bound = 1924
#year_upper_bound = 2004
#day_lower_bound = 1
#day_upper_bound =

for guess_attempt in range(5):
   # print(f"this is {guess_attempt + 1}")

    guess_m = randint(1,12)
    guess_yyyy = randint(1924,2004)
    #day_upper_bound = month_days[guess_m][1]
    #guess_dd = randint()

    print(f"{username}, were you born in {guess_m}/{guess_yyyy}")
    guess_y_n = input("Yes or no?")
    if guess_y_n == "yes" or guess_y_n == "Yes":
        exit("I  knew it!")
    elif guess_attempt == 4:
        exit("I have other things to do. Good bye.")
    elif guess_attempt != 4 and guess_y_n == "No" or guess_y_n == "no":
        print("Drat! Lemme try again!")
